/* 
    NAMING CONVENTIONS
    Models - Capitalized (i.e., models folder > User.js)
    Routes - small caps (i.e., routes folder > user.js)
*/

// Dependencies
const express = require("express");
const userController = require("../controllers/user");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// [Routes]

// Check Email
router.post("/checkEmail", (req, res) => {
  userController
    .checkEmailExists(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// Register a User
router.post("/register", (req, res) => {
  userController
    .registerUser(req.body)
    .then((resultFromController) => res.send(resultFromController));
});

// User Authentication
router.post("/login", userController.loginUser);

// [ACTIVITIY] User Details
router.post("/details", verify, userController.getProfile);

// Route to enroll a verified user to a course
router.post("/enroll", verify, userController.enroll);

// [Export Route System]
module.exports = router;
