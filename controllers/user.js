// Controller

const User = require("../models/User");
const Course = require("../models/Course");
const bcrypt = require("bcrypt");
const auth = require("../auth");

// Check if the email exists already
/* 
    Steps:
    1. "Find" - Mongoose method to find duplicate items
    2. "Then" - Method to send a response back to the FE Application based on the result of the "find" method
*/

module.exports.checkEmailExists = (reqbody) => {
  return User.find({ email: reqbody.email }).then((result) => {
    // Find method returns a record if a match is found
    if (result.length > 0) {
      return true;
    } else {
      // No duplicate found
      // User is not yet registered in DB
      return false;
    }
  });
};

// User Registration
/* 
    Business Logic
    - Create a new User object using the Mongoose model & information from req.body
    - Make sure that the password is encrypted
    - Save the new User to the database 
*/

module.exports.registerUser = (reqbody) => {
  let newUser = new User({
    firstName: reqbody.firstName,
    lastName: reqbody.lastName,
    email: reqbody.email,
    mobileNo: reqbody.mobileNo,
    password: bcrypt.hashSync(reqbody.password, 10),
  });
  return newUser
    .save()
    .then((user, error) => {
      if (error) {
        return false;
      } else {
        return true;
      }
    })
    .catch((err) => err);
};

// User Authentication
/* 
    Business Logic
    - Check the DB for if the user email exists
    - Compare the password from req.body and password stored in DB
    - Generate a JWT if the user logged in and return false if not
*/

module.exports.loginUser = (req, res) => {
  return User.findOne({ email: req.body.email })
    .then((result) => {
      if (result === null) {
        return false;
      } else {
        // compareSync is used to compare a non-encrypted password and the encrypted password from the DB
        const isPasswordCorrect = bcrypt.compareSync(
          req.body.password,
          result.password
        );
        // If PW matches
        if (isPasswordCorrect) {
          return res.send({ access: auth.createAccessToken(result) });
        }
        // Else PW does not match
        else {
          return res.send(false);
        }
      }
    })
    .catch((err) => res.send(err));
};

// Retrieve User Details

module.exports.getProfile = (req, res) => {
  return User.findById(req.user.id)
    .then((result) => {
      result.password = "";
      return res.send(result);
    })
    .catch((err) => res.send(err));
};

// Enroll Authenticated User
/* 
  Business Logic
  1. Find the document in the database using the user's ID
  2. Add the course ID to the users enrollment array
  3. Update the document in the MongoDB Atlas Database
*/
module.exports.enroll = async (req, res) => {
  // Confirm user ID and Course ID in terminal
  console.log(req.user.id);
  console.log(req.body.courseId);

  // Check if user is admin
  if (req.user.isAdmin) {
    return res.send("Action Forbidden");
  }

  // Creates an "isUserUpdated" variable and returns true upon successful update; otherwise, returns error
  let isUserUpdated = await User.findById(req.user.id).then((user) => {
    let newEnrollment = {
      courseId: req.body.courseId,
    };

    // Add courseId in an object, and push object into the user's "enrollment" array
    user.enrollments.push(newEnrollment);

    // Save updated user and return true if successful, or error if failed
    return user
      .save()
      .then((user) => true)
      .catch((err) => err.message);
  });

  // Checks if there are errors in updating the user
  if (isUserUpdated !== true) {
    return res.send({ message: isUserUpdated });
  }

  let isCourseUpdated = await Course.findById(req.body.courseId).then(
    (course) => {
      let enrollee = {
        userId: req.user.id,
      };
      course.enrollees.push(enrollee);
      return course
        .save()
        .then((course) => true)
        .catch((err) => err.message);
    }
  );

  // Checks if there are errors in updating the couurse
  if (isCourseUpdated !== true) {
    return res.send({ message: isCourseUpdated });
  }

  // Checks if both user and course update were successful
  if (isUserUpdated && isCourseUpdated) {
    return res.send({ message: "Enrolled successfully." });
  }
};
