// Dependencies
const express = require("express");
const courseController = require("../controllers/course");
const auth = require("../auth");
const { verify, verifyAdmin } = auth;

// Routing Component
const router = express.Router();

// ACTIVITY: Admin Course Creation
router.post("/", verify, verifyAdmin, courseController.addCourse);

// Route for retrieving all courses
router.get("/all", courseController.getAllCourses);

// Mini-Activity: Create a Route for Getting All Active Courses
router.get("/", courseController.getAllActiveCourses);

// Get Specific Course
router.get("/:courseId", courseController.getCourse);

// Edit a Specific Course
router.put("/:courseId", verify, verifyAdmin, courseController.updateCourse);

// Activity: Archive Specific Course
router.put(
  "/:courseId/archive",
  verify,
  verifyAdmin,
  courseController.archiveCourse
);

// Activity: Activate Specific Course
router.put(
  "/:courseId/activate",
  verify,
  verifyAdmin,
  courseController.activateCourse
);


// [Export Route System]
module.exports = router;
