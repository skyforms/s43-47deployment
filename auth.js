const jwt = require("jsonwebtoken");
const secret = "CourseBookingAPI";

// JSON Web Tokens
// A way of securely passing information from the server to the frontend/other parts of the server

/*
    Token Creation: Analogy
    Wrap the gift and provide a lock with the secret code as the key
*/

module.exports.createAccessToken = (user) => {
  // The data from the user will be received through forms/req.body
  // When the user logs in, a token will be created with the user's information

  const data = {
    id: user._id,
    email: user.email,
    isAdmin: user.isAdmin,
  };
  return jwt.sign(data, secret, {});
};

// Token Verification
/* 
  Analogy: Receive the gift and open the lock to verify if the sender is legitimate, and the gift was not tampered with
*/

module.exports.verify = (req, res, next) => {
  // Middleware which have access to req, res, and next - also can send responses to our client
  // Next - a callback to pass control to the next middleware or router
  // Token is retrieved from the request header
  // Provided in postman under Authorization > Bearer Token

  // req.headers.authorization contains sensitive data and especially our token
  console.log("This is from req.headers.authorization");
  console.log(req.headers.authorization);
  let token = req.headers.authorization;

  if (typeof token === "undefined") {
    return res.send({ auth: "Failed. No token" });
  } else {
    // slice() - method which can be used on strings and arrays
    // slice (<startingPosition>, <endPosition>)
    /* 
      Bearer asdfasdfasdf

      "Peter"
      slice(3, string.length)
      "er"
    */
    console.log("With bearer prefix");
    console.log(token);
    token = token.slice(7, token.length);
    console.log("No bearer prefix");
    console.log(token);

    /*
      Token Decryption
      Analogy: Open the gift and get the contents
    */

    jwt.verify(token, secret, function (err, decodedToken) {
      // Validate the token using the verify method, decrypting the token using the secret code
      // err will contain the error from decoding the token - this will contain the reason the token was rejected
      if (err) {
        return res.send({
          auth: "Failed",
          message: err.message,
        });
      } else {
        console.log("Data that will be assigned to the req.user");
        console.log(decodedToken);

        req.user = decodedToken;
        next();
        // Middleware function that lets us proceed to the next middleware/controller
      }
    });
  }
};

module.exports.verifyAdmin = (req, res, next) => {
  // verifyAdmin comes AFTER the verify middleware

  if (req.user.isAdmin) {
    next();
  } else {
    return res.send({
      auth: "Failed",
      message: "Action Forbidden",
    });
  }
};
